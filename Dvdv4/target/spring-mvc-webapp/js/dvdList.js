


$(document).ready(function () {
    loadDVDs();
});


function loadDVDs() {

    clearDVDTable();
    var cTable = $('#contentRows');


    $.each(testDVDData, function (index, dvd) {
        cTable.append($('<tr>')
                .append($('<td>')
                        .append($('<a>').attr({
                            'data-dvd-id': dvd.dvdId,
                            'data-toggle': 'modal',
                            'data-target': '#detailModal'
                        }).text(dvd.dvdTitle + ' ' + dvd.releaseDate)
                                )
                        )
                //from releaseDate to mpaaRating
                .append($('<td>').text(dvd.mpaaRating))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-dvd-id': dvd.dvdId,
                                    'data-toggle': 'modal',
                                    'data-target': '#editModal'
                                }).text('Edit')

                                )
                        )
                .append($('<td>').remove()
                        .append($('<td>')
                                .append($('<a>')
                                        .attr({
                                            'data-dvd-id': dvd.remove,
                                            'data-toggle': 'modal',
                                            'data-target': '#deleteModal'
                                        }).text('Delete')

                                        )

                                )
                        ));
    });
}


function clearDVDTable() {
    $('#contentRows').empty();
}


$('#detailModal').on('show.bs.modal', function (event) {
    
    var element = $(event.relatedTarget);

    var dvdId = element.data('dvd-id');


    var modal = $(this);
    modal.find('#dvd-dvdId').text(dummyDetailDVD.dvdId);
    modal.find('#dvd-dvdTitle').text(dummyDetailDVD.dvdTitle);
    modal.find('#dvd-releaseDate').text(dummyDetailDVD.releaseDate);
    modal.find('#dvd-mpaaRating').text(dummyDetailDVD.mpaaRating);
    modal.find('#dvd-notes').text(dummyDetailDVD.notes);
    modal.find('#dvd-director').text(dummyDetailDVD.director);
    modal.find('#dvd-studio').text(dummyDetailDVD.studio);

});


$('#editModal').on('show.bs.modal', function (event) {


    var element = $(event.relatedTarget);

    var dvdId = element.data('dvd-dvdId');



    var modal = $(this);
    modal.find('#edit-dvdId').text(dummyEditDVD.dvdId);
    modal.find('#edit-dvdTitle').val(dummyEditDVD.dvdTitle);
    modal.find('#edit-releaseDate').val(dummyEditDVD.releaseDate);
    modal.find('#edit-mpaaRating').val(dummyEditDVD.mpaaRating);
    modal.find('#edit-notes').val(dummyEditDVD.notes);
    modal.find('#edit-director').val(dummyEditDVD.director);
    modal.find('#edit-studio').val(dummyEditDVD.studio);

});

$('#deleteModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);

    var dvdId = element.data('dvd-dvdId');
    


});



var testDVDData = [
    {
        dvdId: 1,
        dvdTitle: "Shrek",
        releaseDate: "2000",
        mpaaRating: "PG",
        notes: "7/10",
        director: "James Shoeman",
        studio: "Spy Glass"
    },
    {
        dvdId: 2,
        dvdTitle: "Nemo",
        releaseDate: "2005",
        mpaaRating: "PG",
        notes: "8/10",
        director: "Luke Manyways",
        studio: "Warner Bros"
    },
    {
        dvdId: 3,
        dvdTitle: "Road Trip",
        releaseDate: "2003",
        mpaaRating: "R",
        notes: "4/10",
        director: "Cameron Hoha",
        studio: "Looks"
    },
    {
        dvdId: 4,
        dvdTitle: "Hangover",
        releaseDate: "2008",
        mpaaRating: "R",
        notes: "10/10",
        director: "Shimmy Shoeshine",
        studio: "Duke Kils"
    }
];

var dummyDetailDVD =
        {
            dvdId: 42,
            dvdTitle: "Lipton",
            releaseDate: "2000",
            mpaaRating: "PG",
            notes: "7/10",
            director: "James Shoeman",
            studio: "Spy Glass"
        };

var dummyEditDVD =
        {
            dvdId: 32,
            dvdTitle: "Hipster",
            releaseDate: "2008",
            mpaaRating: "R",
            notes: "10/10",
            director: "Shimmy Shoeshine",
            studio: "Duke Kils"
        };