/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dvdv4.dao;

/**
 *
 * @author apprentice
 */
public enum SearchTerm {
    DVD_TITLE, MPAA_RATING, NOTES, DIRECTOR, STUDIO, RELEASE_DATE
}
