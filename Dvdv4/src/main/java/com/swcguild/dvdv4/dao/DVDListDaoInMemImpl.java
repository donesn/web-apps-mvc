package com.swcguild.dvdv4.dao;

import com.swcguild.dvdv4.model.DVD;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DVDListDaoInMemImpl implements DVDListDao {

    private Map<Integer, DVD> dvdMap = new HashMap<>();
    private static int dvdIdCounter = 0;

    @Override
    public DVD addDVD(DVD dvd) {
        dvd.setDvdId(dvdIdCounter);
        dvdIdCounter++;
        dvdMap.put(dvd.getDvdId(), dvd);
        return dvd;
    }

    @Override
    public void removeDVD(int dvdId) {
        dvdMap.remove(dvdId);
    }

    @Override
    public void updateDVD(DVD dvd) {
        dvdMap.put(dvd.getDvdId(), dvd);
    }

    @Override
    public List<DVD> getAllDVD() {
        Collection<DVD> c = dvdMap.values();
        return new ArrayList(c);
    }

    @Override
    public DVD getDVDById(int dvdId) {
        return dvdMap.get(dvdId);
    }

    @Override
    public List<DVD> searchDVD(Map<SearchTerm, String> criteria) {

        String dvdTitleCriteria = criteria.get(SearchTerm.DVD_TITLE);
        String releaseDateCriteria = criteria.get(SearchTerm.RELEASE_DATE);
        String mpaaRatingCriteria = criteria.get(SearchTerm.MPAA_RATING);
        String notesCriteria = criteria.get(SearchTerm.NOTES);
        String directorCriteria = criteria.get(SearchTerm.DIRECTOR);
        String studioCriteria = criteria.get(SearchTerm.STUDIO);

        Predicate<DVD> dvdTitleMatches;
        Predicate<DVD> releaseDateMatches;
        Predicate<DVD> mpaaRatingMatches;
        Predicate<DVD> notesMatches;
        Predicate<DVD> directorMatches;
        Predicate<DVD> studioMatches;

        Predicate<DVD> truePredicate = (c) -> {
            return true;
        };

        dvdTitleMatches = (dvdTitleCriteria == null || dvdTitleCriteria.isEmpty())
                ? truePredicate : (c) -> c.getDvdTitle().equals(dvdTitleCriteria);
        releaseDateMatches = (releaseDateCriteria == null || releaseDateCriteria.isEmpty())
                ? truePredicate : (c) -> c.getReleaseDate().equals(releaseDateCriteria);
        mpaaRatingMatches = (mpaaRatingCriteria == null || mpaaRatingCriteria.isEmpty())
                ? truePredicate : (c) -> c.getMpaaRating().equals(mpaaRatingCriteria);
        notesMatches = (notesCriteria == null || notesCriteria.isEmpty())
                ? truePredicate : (c) -> c.getNotes().equals(notesCriteria);
        directorMatches = (directorCriteria == null || directorCriteria.isEmpty())
                ? truePredicate : (c) -> c.getDirector().equals(directorCriteria);
        studioMatches = (studioCriteria == null || studioCriteria.isEmpty())
                ? truePredicate : (c) -> c.getStudio().equals(studioCriteria);

        return dvdMap.values().stream()
                .filter(dvdTitleMatches
                        .and(releaseDateMatches)
                        .and(mpaaRatingMatches)
                        .and(notesMatches)
                        .and(directorMatches)
                        .and(studioMatches)
                ).collect(Collectors.toList());
    }

}
