<!-- Details Modal -->
<div class="modal fade" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="detailModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="detailModalLabel">DVD Details</h4>
            </div>
            <div class="modal-body">
                <h3 id="dvd-id"></h3>
                <table class="table table-bordered">
                    <tr>
                        <th>DVD Title:</th>
                        <td id="dvd-dvdTitle"></td>
                    </tr>
                    <tr>
                        <th>Release Date:</th>
                        <td id="dvd-releaseDate"></td>
                    </tr>
                    <tr>
                        <th>MPAA Rating:</th>
                        <td id="dvd-mpaaRating"></td>
                    </tr>
                    <tr>
                        <th>Notes:</th>
                        <td id="dvd-notes"></td>
                    </tr>
                    <tr>
                        <th>Director:</th>
                        <td id="dvd-director"></td>
                    </tr>
                    <tr>
                        <th>Studio:</th>
                        <td id="dvd-studio"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="editModalLabel">Edit DVD</h4>
            </div>
            <div class="modal-body">
                <h3 id="dvd-id"></h3>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="edit-dvdTitle" class="col-md-4 control-label">DVD Title:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="edit-dvdTitle" placeholder="DVD Title" /></div>
                    </div>
                    <div class="form-group">
                        <label for="edit-releaseDate" class="col-md-4 control-label">Release Date:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="edit-releaseDate" placeholder="Release Date" /></div>
                    </div>
                    <div class="form-group">
                        <label for="edit-mpaaRating" class="col-md-4 control-label">MPAA Rating:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="edit-mpaaRating" placeholder="MPAA Rating" /></div>
                    </div>
                    <div class="form-group">
                        <label for="edit-notes" class="col-md-4 control-label">Notes:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="edit-notes" placeholder="Notes" /></div>
                    </div>
                    <div class="form-group">
                        <label for="edit-director" class="col-md-4 control-label">Director:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="edit-director" placeholder="Director" /></div>
                    </div>
                    <div class="form-group">
                        <label for="edit-studio" class="col-md-4 control-label">Studio:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="edit-studio" placeholder="Studio" /></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8">
                            <button type="submit" id="edit-button" class="btn btn-default" data-dismiss="modal">Edit DVD</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <input type="hidden" id="dvd-id"/>
                        </div>
                    </div>

                    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">&times;</span>
                                        <span class="sr-only">Close</span>
                                    </button>
                                    <h4 class="modal-title" id="deleteModalLabel">Delete DVD</h4>
                                </div>
                                <div class="modal-body">
                                    <h3 id="dvd-id"></h3>
                                    <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label for="delete-dvdTitle" class="col-md-4 control-label">DVD Title:</label>
                                            <div class="col-md-8"><input type="text" class="form-control" id="delete-dvdTitle" placeholder="DVD Title" /></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="delete-releaseDate" class="col-md-4 control-label">Release Date:</label>
                                            <div class="col-md-8"><input type="text" class="form-control" id="delete-releaseDate" placeholder="Release Date" /></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="delete-mpaaRating" class="col-md-4 control-label">MPAA Rating:</label>
                                            <div class="col-md-8"><input type="text" class="form-control" id="delete-mpaaRating" placeholder="MPAA Rating" /></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="delete-notes" class="col-md-4 control-label">Notes:</label>
                                            <div class="col-md-8"><input type="text" class="form-control" id="delete-notes" placeholder="Notes" /></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="delete-director" class="col-md-4 control-label">Director:</label>
                                            <div class="col-md-8"><input type="text" class="form-control" id="delete-director" placeholder="Director" /></div>
                                        </div>
                                        <div class="form-group">
                                            <label for="delete-studio" class="col-md-4 control-label">Studio:</label>
                                            <div class="col-md-8"><input type="text" class="form-control" id="delete-studio" placeholder="Studio" /></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-offset-4 col-md-8">
                                                <button type="submit" id="-button" class="btn btn-default" data-dismiss="modal">Delete DVD</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                                <input type="hidden" id="dvd-id"/>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
