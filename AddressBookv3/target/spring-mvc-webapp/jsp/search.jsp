<%-- 
    Document   : search
    Created on : Oct 28, 2015, 11:38:12 AM
    Author     : apprentice
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Address Book</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/img/icon.png" rel="shortcut icon">
    </head>
    <body>
        <div class="container">
            <h1>Company Contacts</h1>
            <hr/>
            <div class ="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/home">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                </ul>
            </div>
        </div>

        <div class="row">
            <div class="col-md-5 col-md-offset-1">
                <h2>My Contacts</h2>
                <%@include file="contactSummaryTableFragment.jsp"%>
            </div> <!-- end col-md-6 div w/ table -->
            <div class="col-md-5">
                <h2>Search</h2>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="search-firstName" class="col-md-4 control-label">First Name:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="search-firstName" placeholder="First Name"/></div>
                    </div>
                    <div class="form-group">
                        <label for="search-lastName" class="col-md-4 control-label">Last Name:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="search-lastName" placeholder="Last Name" /></div>
                    </div>
                    <div class="form-group">
                        <label for="search-streetAddress" class="col-md-4 control-label">Street Address:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="search-streetAddress" placeholder="Street Address" /></div>
                    </div>
                    <div class="form-group">
                        <label for="search-city" class="col-md-4 control-label">City:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="search-city" placeholder="City"/></div>
                    </div>
                    <div class="form-group">
                        <label for="search-state" class="col-md-4 control-label">State:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="search-state" placeholder="State" /></div>
                    </div>
                    <div class="form-group">
                        <label for="search-zipcode" class="col-md-4 control-label">Zipcode:</label>
                        <div class="col-md-8"><input type="text" class="form-control" id="search-zipcode" placeholder="Zipcode" /></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8"><button type="submit" id="search-button" class="btn btn-default">Search Contact</button></div>
                    </div>
                </form>
            </div>
        </div>

        <%@include file="detailEditModalFragment.jsp"%>

        <!-- placed at the end of the document so that the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/contactList.js"></script>
    </body>
</html>
