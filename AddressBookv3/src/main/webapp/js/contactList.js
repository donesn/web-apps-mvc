


$(document).ready(function () {
    loadContacts();
});


function loadContacts() {

    clearContactTable();
    var cTable = $('#contentRows');


    $.each(testContactData, function (index, contact) {
        cTable.append($('<tr>')
                .append($('<td>')
                        .append($('<a>').attr({
                            'data-contact-id': contact.contactId,
                            'data-toggle': 'modal',
                            'data-target': '#detailModal'
                        }).text(contact.firstName + ' ' + contact.lastName)
                                )
                        )
                .append($('<td>').text(contact.streetAddress))
                .append($('<td>')
                        .append($('<a>')
                                .attr({
                                    'data-contact-id': contact.contactId,
                                    'data-toggle': 'modal',
                                    'data-target': '#editModal'
                                }).text('Edit')

                                )
                        )
                .append($('<td>').remove()
                        .append($('<td>')
                                .append($('<a>')
                                        .attr({
                                            'data-contact-id': contact.remove,
                                            'data-toggle': 'modal',
                                            'data-target': '#deleteModal'
                                        }).text('Delete')

                                        )

                                )
                        ));
    });
}


function clearContactTable() {
    $('#contentRows').empty();
}


$('#detailModal').on('show.bs.modal', function (event) {


    var element = $(event.relatedTarget);

    var contactId = element.data('contact-id');



    var modal = $(this);
    modal.find('#contact-id').text(dummyDetailContact.contactId);
    modal.find('#contact-firstName').text(dummyDetailContact.firstName);
    modal.find('#contact-lastName').text(dummyDetailContact.lastName);
    modal.find('#contact-streetAddress').text(dummyDetailContact.streetAddress);
    modal.find('#contact-city').text(dummyDetailContact.city);
    modal.find('#contact-state').text(dummyDetailContact.state);
    modal.find('#contact-zipcode').text(dummyDetailContact.zipcode);

});


$('#editModal').on('show.bs.modal', function (event) {


    var element = $(event.relatedTarget);

    var contactId = element.data('contact-id');



    var modal = $(this);
    modal.find('#contact-id').text(dummyEditContact.contactId);
    modal.find('#edit-firstName').val(dummyEditContact.firstName);
    modal.find('#edit-lastName').val(dummyEditContact.lastName);
    modal.find('#edit-streetAddress').val(dummyEditContact.streetAddress);
    modal.find('#edit-city').val(dummyEditContact.city);
    modal.find('#edit-state').val(dummyEditContact.state);
    modal.find('#edit-zipcode').val(dummyEditContact.zipcode);

});

$('#deleteModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);

    var contactId = element.data('contact-id');



    var modal = $(this);
    modal.find('#contact-id').text(dummyDeleteContact.contactId);
    modal.find('#delete-firstName').val(dummyDeleteContact.firstName);
    modal.find('#delete-lastName').val(dummyDeleteContact.lastName);
    modal.find('#delete-streetAddress').val(dummyDeleteContact.streetAddress);
    modal.find('#delete-city').val(dummyDeleteContact.city);
    modal.find('#delete-state').val(dummyDeleteContact.state);
    modal.find('#delete-zipcode').val(dummyDeleteContact.zipcode);

});



var testContactData = [
    {
        contactId: 1,
        firstName: "Billy",
        lastName: "TwoPipes",
        streetAddress: "123 Main St.",
        city: "Lexington",
        state: "KY",
        zipcode: "40502"
    },
    {
        contactId: 2,
        firstName: "Darth",
        lastName: "Vader",
        streetAddress: "456 East Market St.",
        city: "Louisville",
        state: "KY",
        zipcode: "40204"
    },
    {
        contactId: 3,
        firstName: "Han",
        lastName: "Solo",
        streetAddress: "789 Arizona Ave.",
        city: "Florence",
        state: "KY",
        zipcode: "41022"
    },
    {
        contactId: 4,
        firstName: "Luke",
        lastName: "Skywalker",
        streetAddress: "578 Phoenix Blvd.",
        city: "Lexington",
        state: "KY",
        zipcode: "40503"
    }
];

var dummyDetailContact =
        {
            contactId: 58,
            firstName: "Joda",
            lastName: "Wars",
            streetAddress: "987 W Wesport Ave.",
            city: "Louisville",
            state: "KY",
            zipcode: "40678"
        };

var dummyEditContact =
        {
            contactId: 67,
            firstName: "Mace",
            lastName: "Windu",
            streetAddress: "298 Linux Dr.",
            city: "Springfield",
            state: "KY",
            zipcode: "40256"
        };