/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.addressbookv3.dao;

/**
 *
 * @author apprentice
 */
public enum SearchTerm {
    FIRST_NAME, LAST_NAME, CITY, STATE, ZIPCODE, STREET_ADDRESS
}
