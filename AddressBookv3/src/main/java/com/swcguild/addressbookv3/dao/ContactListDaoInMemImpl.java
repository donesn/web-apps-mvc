/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.addressbookv3.dao;

import com.swcguild.addressbookv3.model.Contact;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class ContactListDaoInMemImpl implements ContactListDao {

    private Map<Integer, Contact> contactMap = new HashMap<>();
    private static int contactIdCounter = 0;

    @Override
    public Contact addContact(Contact contact) {
        contact.setContactId(contactIdCounter);
        contactIdCounter++;
        contactMap.put(contact.getContactId(), contact);
        return contact;
    }

    @Override
    public void removeContact(int contactId) {
        contactMap.remove(contactId);
    }

    @Override
    public void updateContact(Contact contact) {
        contactMap.put(contact.getContactId(), contact);
    }

    @Override
    public List<Contact> getAllContacts() {
        Collection<Contact> c = contactMap.values();
        return new ArrayList(c);
    }

    @Override
    public Contact getContactById(int contactId) {
        return contactMap.get(contactId);
    }

    @Override
    public List<Contact> searchContacts(Map<SearchTerm, String> criteria) {

        String firstNameCriteria = criteria.get(SearchTerm.FIRST_NAME);
        String lastNameCriteria = criteria.get(SearchTerm.LAST_NAME);
        String streetAddressCriteria = criteria.get(SearchTerm.STREET_ADDRESS);
        String cityCriteria = criteria.get(SearchTerm.CITY);
        String stateCriteria = criteria.get(SearchTerm.STATE);
        String zipcodeCriteria = criteria.get(SearchTerm.ZIPCODE);

        Predicate<Contact> firstNameMatches;
        Predicate<Contact> lastNameMatches;
        Predicate<Contact> streetAddressMatches;
        Predicate<Contact> cityMatches;
        Predicate<Contact> stateMatches;
        Predicate<Contact> zipcodeMatches;

        Predicate<Contact> truePredicate = (c) -> {
            return true;
        };

        firstNameMatches = (firstNameCriteria == null || firstNameCriteria.isEmpty())
                ? truePredicate : (c) -> c.getFirstName().equals(firstNameCriteria);
        lastNameMatches = (lastNameCriteria == null || lastNameCriteria.isEmpty())
                ? truePredicate : (c) -> c.getLastName().equals(lastNameCriteria);
        streetAddressMatches = (streetAddressCriteria == null || streetAddressCriteria.isEmpty())
                ? truePredicate : (c) -> c.getStreetAddress().equals(streetAddressCriteria);
        cityMatches = (cityCriteria == null || cityCriteria.isEmpty())
                ? truePredicate : (c) -> c.getCity().equals(cityCriteria);
        stateMatches = (stateCriteria == null || stateCriteria.isEmpty())
                ? truePredicate : (c) -> c.getState().equals(stateCriteria);
        zipcodeMatches = (zipcodeCriteria == null || zipcodeCriteria.isEmpty())
                ? truePredicate : (c) -> c.getZipcode().equals(zipcodeCriteria);

        return contactMap.values().stream()
                .filter(firstNameMatches
                        .and(lastNameMatches)
                        .and(streetAddressMatches)
                        .and(cityMatches)
                        .and(stateMatches)
                        .and(zipcodeMatches)
                ).collect(Collectors.toList());
    }

}
