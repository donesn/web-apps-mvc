<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Home</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom styles for this template -->
        <link href="${pageContext.request.contextPath}/css/starter-template.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/my.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">


    </head>
    <body>

        <div id="backCol" class="container">
            <h1 align="center">Vending Machine</h1>
            <hr/>


            <br/>
            <hr/>
            <div id="row" class="row">

                <!--                LEFT PANEL-->
                <div class="col-md-4 col-md-offset-1">  
                    <table id="table1" class="table">
                        <c:forEach items="${items}" var="item"> 
                            <c:set var="count" value="${count + 1}" scope="page"/>
                            <tr>
                                <c:choose>
                                    <c:when test="${count % 2 == 0}">
                                        <td id="item${item.name}" class="success">  ${item.name}</td>
                                        <td id="price${item.name}" class="success">$${item.price}</td>
                                        <td id="inventory${item.name}" class="success">${item.inventory}</td>
                                    </c:when>     
                                    <c:otherwise>
                                        <td id="item${item.name}" class="info">  ${item.name}</td>
                                        <td id="price${item.name}" class="info">$${item.price}</td>
                                        <td id="inventory${item.name}" class="info">${item.inventory}</td>
                                    </c:otherwise>
                                </c:choose>
                            </tr>

                        </c:forEach> 
                    </table>   
                </div>


                <!-- LEFT PANEL -->
                <!--              //////////////////////////////////////////////////////////////////////////-->



                <div id="RContainer" class="col-md-3 col-md-offset-3"><!--  RIGHT PANEL-->
                    <form action="buySelect" method="Post">
                        <select id="itemDropdown" name="items">
                            <c:forEach items="${items}" var="item">
                                <option value="${item.name}">${item.name}</option>
                            </c:forEach> 
                        </select>
                        <div align="center">
                            <h3>Insert Cash</h3>
                            <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                        </div>
                        <div id="inputAni" class="form-group">
                            <input type="cash" class="form-control" name="amount" id="cash" placeholder="$">
                        </div>
                        <div align="center">
                            <button id="pay-button" type="submit" value='Submit' class="btn btn-default btn-info">Pay</button>
                        </div>
               
                </form>
                <div > <p id="changeHome"></p> </div>
                 </div>

                <!--  RIGHT PANEL-->

            </div>

        </div> <!-- CONTAINER-->

        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/my.js"></script>
    </body>
</html>

