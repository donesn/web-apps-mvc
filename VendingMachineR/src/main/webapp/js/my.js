/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {

    $('#pay-button').click(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'buySelect',
            data: JSON.stringify({
                amount: $('#cash').val(),
                p: {
                    name: $('#itemDropdown').val()
                }
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'text'
        }).success(function (data, status) {
            $('#changeHome').text(data);//change text under pay button
            fillTableAjax();//ajax call reload table
        });
    });
});

function clearTable() {
    $('#table1').empty();
}

function fillTableAjax() {
    var pTable = $('#table1');

    $.ajax({
        type: 'GET',
        url: 'updateTable'
    }).success(function (data, status) {
        fillTable(data, status);
    });
}
function fillTable(productList, status) {
    
    var pTable = $('#table1');

    $.each(productList, function (index, product) {
        $('#inventory' + product.name).text(product.inventory);
    });
}





