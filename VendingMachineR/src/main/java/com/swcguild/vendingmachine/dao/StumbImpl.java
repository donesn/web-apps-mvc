/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.vendingmachine.dao;

import com.swcguild.vendingmachine.dto.Product;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class StumbImpl{

//    Map<String, Product> itemsMap = new HashMap<>();
    
    
    private static final String SQL_UPDATE_PRODUCTS = "UPDATE products SET inventory = ? WHERE name = ?";
    private static final String SQL_SELECT_ALL_PRODUCTS= "SELECT * FROM products";
    
    private JdbcTemplate jdbcTemplate;
    
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }
    

    public void updateProduct(String name, int inventory) {
        jdbcTemplate.update(SQL_UPDATE_PRODUCTS,
            inventory, name);
           
    }
    
        
    public List<Product> getVendingMachineStatus() {
        return jdbcTemplate.query(SQL_SELECT_ALL_PRODUCTS, new ContactMapper());
    }
    
    
        private static final class ContactMapper implements RowMapper<Product>{

        @Override
        public Product mapRow(ResultSet rs, int i) throws SQLException {
            Product product = new Product();
            
            product.setName(rs.getString("name"));
            product.setPrice(rs.getDouble("price"));
            product.setInventory(rs.getInt("inventory"));

            
            return product;
        }
        
    }
    
    
    
    
//    public StumbImpl(){
//
////        itemsMap.put("Mars", new Product("Mars", 2.00, 5));
////        itemsMap.put("Snickers", new Product("Snickers", 1.5, 3));
////        itemsMap.put("M&M", new Product("M&M", 2.00, 2));
////        itemsMap.put("Cheetos", new Product("Cheetos", 2.50, 0));
////        itemsMap.put("Doritos", new Product("Doritos", 2.75, 1));
////        itemsMap.put("Twix", new Product("Twix", 1.50, 5));
////        itemsMap.put("Coke Cola", new Product("Coke Cola", 1.00, 5));
////        itemsMap.put("Water", new Product("Water", 0.50, 5));
////        itemsMap.put("Sprite", new Product("Sprite", 1.00, 5));
////        itemsMap.put("Fanta", new Product("Fanta", 1.00, 5));
////        itemsMap.put("Reeses", new Product("Reeses", 1.75, 5));
////        itemsMap.put("KitKat", new Product("KitKat", 2.00, 5));
////        itemsMap.put("Hershey", new Product("Hershey", 2.50, 5));
////        itemsMap.put("Starbust", new Product("Starbust", 3.00, 5));
////        itemsMap.put("Skittles", new Product("Skittles", 2.00, 5));
//   
//    }
    
//    public List<Product> getAllItems(){
//     
//        
//        return new ArrayList(itemsMap.values());
//    }
//    
//    public double getPrice(String name){
//        
//       return itemsMap.get(name).getPrice();
//    }
}
