/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.vendingmachine.controller;

import com.swcguild.vendingmachine.dao.StumbImpl;
import com.swcguild.vendingmachine.dto.Product;
import com.swcguild.vendingmachine.dto.ProductWith;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 *
 * @author apprentice
 */
@Controller
public class Home extends HttpServlet {

    private int quarter;
    private int dime;
    private int nickel;
    private int penny;
    private StumbImpl dao;

    double amount;

    @Inject
    public Home(StumbImpl items) {
        this.dao = items;
    }

    @RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
    public String displayHomePage(Model model) {

        List<Product> allItems = dao.getVendingMachineStatus();
        model.addAttribute("items", allItems);
        return "home";
    }

    @RequestMapping(value = "/buySelect", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public String buySelect(@Valid @RequestBody ProductWith productwith) throws IOException, ServletException {
        double amount = productwith.getAmount();
        double price = getItemPrice(productwith.getName());

        if (itemExist(productwith.getName())) {
            if (amount >= price) {
                dao.updateProduct(productwith.getName(), updateInventory(productwith.getName()));//reduce inventory
                return changer(price, amount);//gives change
            } else {
                return "Not enough funds";
            }
        } else {
            return "Out of stock";
        }
    }

    @RequestMapping(value = "/updateTable", method = RequestMethod.GET)
    @ResponseBody
    public List<Product> getAllProducts() {
        return dao.getVendingMachineStatus();
    }

    public double getItemPrice(String name) {
        List<Product> allItems = dao.getVendingMachineStatus();
        double price = 0;
//       List<Product> item = allItems.stream()
//                 .filter(k -> k.getName().equalsIgnoreCase(name))
//                 .collect(Collectors.toList());
//       
//       double price = item.get(1).getPrice();

        for (int i = 0; i < allItems.size(); i++) {
            if (allItems.get(i).getName().equalsIgnoreCase(name)) {
                price = allItems.get(i).getPrice();
            }
        }

        return price;
    }

    public int updateInventory(String name) {
        List<Product> allItems = dao.getVendingMachineStatus();
        int inventory = -1;
        for (int i = 0; i < allItems.size(); i++) {
            if (allItems.get(i).getName().equalsIgnoreCase(name)) {
                inventory = allItems.get(i).getInventory();
            }
        }
        return inventory - 1;

    }

    public boolean itemExist(String name) {
        List<Product> allItems = dao.getVendingMachineStatus();

        return allItems.stream()
                .anyMatch(k -> k.getName().equalsIgnoreCase(name) && k.getInventory() > 0);

    }

    public String changer(double priceNotP, double inputNotP) {

        inputNotP = inputNotP * 100;
        priceNotP = priceNotP * 100;

        int input = (int) inputNotP;
        int price = (int) priceNotP;
        int change = input - price;
        String MoneyReturn;

        String SQuarter = "";
        String SDime = "";
        String SNickel = "";
        String SPenny = "";

        //QUARTER
        if (change >= 25) {

            int i;
            for (i = 1; i < (change / 25); i++) {
            }

            quarter = (int) (i);

            change = change - (quarter * 25);
            SQuarter = " " + Integer.toString(quarter) + " Quarter\n";
        }

        //DIME
        if (change >= 10) {
            int i;
            for (i = 1; i < (change / 10); i++) {
            }

            dime = (int) (i);

            change = change - (dime * 10);
            SDime = " " + Integer.toString(dime) + " dime\n";
        }

        //NICKEL
        if (change >= 5) {
            int i;
            for (i = 1; i < (change / 5); i++) {
            }

            nickel = (int) (i);

            change = change - (nickel * 5);
            SNickel = " " + Integer.toString(nickel) + " nickel\n";
        }

        //PENNY
        if (change >= 1) {
            int i;
            for (i = 1; i < (change / 1); i++) {
            }

            penny = (int) (i);

            change = change - (penny * 1);
            SPenny = " " + Integer.toString(penny) + " penny\n";
        }

        String allTogether = SQuarter + SDime + SNickel + SPenny;
        return allTogether;

    }

    //    @RequestMapping(value = "/buySelect", method = RequestMethod.POST)
//    public String buySelect(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
//
//        String itemChosen = request.getParameter("items");
//        request.setAttribute("itemChosen", itemChosen);
//        
//        double amount = Double.parseDouble(request.getParameter("amount"));
//        double price = dao.getPrice(itemChosen);
//        
//        String itemChange = changer(price, amount);
//        
//        request.setAttribute("itemChange", itemChange);
//        return "change";
//    }
}
